/*=========================================================================================
  File Name: actions.js
  Description: Vuex Store - actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import { themeActions } from './themeActions'
import axios from 'axios'

axios.defaults.baseURL = '/api'

const actions = {
  ...themeActions,

  async login ({ commit }, credentials) {
    const { data } = await axios.post('/login', credentials)
    commit('setUserData', data)
  },

  logout ({ commit }) {
    commit('clearUserData')
  }
}

export default actions
